MOKIT 1.2.3 (Jan 18, 2022)
------------------------
* update radical/tetraradical index formulae
* fix a bug of py2fch when using small basis set like STO-3G/6G, 3-21G
* output Head-Gordon's unpaired electrons, biradical/tetraradical index y0/y1
* add keyword CASPT2K (interfaced with ORCA 5.x)
* compute +Q Davidson correction for MRCISD in OpenMolcas/GAMESS
* add keyword excludeXH and a utility gvb_exclude_XH
* supplement pairs when UNO pairs are not enough (commented)
* compatible keywords and scripts with Molpro 2021 and GAMESS 2021
* use minimal basis set def2-SVP (in RHF proj) for atoms>Xe, instead of STO-6G
* create all possible Python APIs in rwgeom.f90
* add GVB, OVB-MP2 interface of Gaussian
* add FIC-MRCC interface of ORCA(>=5.0.0)
* add SAPT0 PSI4 interface in utility frag_guess_wfn
* add keyword NMR for calculating nuclear shielding constants using Dalton
* add utilities bas_gms2dal, fch2dal and dal2fch (Gaussian <-> Dalton)
* put basis sets ANO-RCC-VDZP, ANO-RCC-VTZP and ANO-RCC-VQZP into basis/
* remove Sdiag in fch2py and py2fch, add write_pyscf_dm_into_fch
* change generated files of fch2mkl to *_o.inp and *_o.mkl
* extend utility frag_guess_wfn to generate GAMESS EDA input
* add utility fch2qm4d (Gaussian -> QM4D)
* add DFT/MRCI interfaces
* add keyword HF_prog (=Gaussian, PSI4, ORCA)

MOKIT 1.2.2 (Feb 24, 2021)
------------------------
* add CAS and MRCISD interfaces between AutoMR and PSI4
* add utilities bas_gms2psi, fch2psi and replace_xyz_in_inp
* add guess(fragment=N) support for input gjf
* add utilities bas_gau2molcas
* create directory basis to hold jkfit and some relativistic basis sets
* add RI, F12, DLPNO interfaces
* add Python APIs
* generate correct density in .fch for GVB and CAS
* add utility mkl2gjf
* add utility bdf2mkl (BDF -> ORCA)
* read orbital energies in utilities fch2bdf, bdf2fch, mkl2fch
* add CASPT3 and NEVPT3 interfaces
* add CAS, NEVPT2 and SDSPT2 interfaces of BDF
* add utility bdf2fch (BDF -> Gaussian)
* add utilities bas_gms2bdf and fch2bdf (Gaussian -> BDF)
* add MC-PDFT interface of GAMESS(>=2019R2)
* add NEVPT2 interface of Molpro
* add DKH2 and X2C interfaces of Molpro
* add background point charge interface of Molpro
* add CASSCF force interface of Molpro
* allow specification of active space (m,m) in dynamic correlation

MOKIT 1.2.1 (2020-12-15)
------------------------
* add interfaces for Molpro CASPT2(RS2C) and MRCISD(MRCIC)
* add utility xml2fch (Molpro -> Gaussian)
* read more digits of NOONs in some cases
* add interface for Molpro CASSCF
* add utilities bas_gms2molpro and fch2com (Gaussian -> Molpro)
* fix some Gfortran compiling incompatibility
* remove file program.info, read $ORCA and $GMS from environment variables
* add interfaces for MRMP2, MC-PDFT and DMRG-PDFT
* add keywords DKH2 and X2C
* fix the bug of utility dat2fch when '-no' specified
* update utilities fch2inp and dat2fch to support spherical harmonic functions for GAMESS
* change the keyword 'nocart'->'cart', now default spherical harmonic functions

MOKIT 1.1 beta (2020-09-25)
------------------------
* add interfaces for uc-/ic-/FIC-MRCISD
* add utility orb2fch
* fix bug: in fch2inp when gvb=True and nopen>3
* fix bug: 1 pair case in gvb_sort_pairs

MOKIT 1.1 alpha (2020-08-09)
------------------------
* support background point charges
* fix the bug of keyword 'force'
* add conditional compilation for Windows
* add pre-compiled Windows executables

MOKIT 1.0 beta (2020-06-28)
------------------------
* add program AutoMR
* fix the bug in fch2mkl
* add utility mkl2fch
* add the manual for MOKIT

MOKIT 1.0 alpha (2020-05-05)
------------------------
* Setup MOKIT
